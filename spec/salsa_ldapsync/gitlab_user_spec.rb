require 'salsa_ldapsync/gitlab_user'

describe SalsaLdapsync::GitlabUser do
  describe '.initialize' do
    it do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'active' }
      allow(info).to receive(:to_h) { { 'custom_attributes' => [] } }

      client = instance_double(Gitlab::Client)

      u = SalsaLdapsync::GitlabUser.new(client, info)
      expect(u)
    end
  end

  describe '.state=' do
    it 'no action on same state' do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'active' }
      allow(info).to receive(:to_h) { { 'custom_attributes' => [] } }

      client = instance_double(Gitlab::Client)

      u = SalsaLdapsync::GitlabUser.new(client, info)
      expect(u.state = 'active').to eq('active')
    end

    it 'blocks user' do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'active' }
      allow(info).to receive(:to_h) { { 'custom_attributes' => [] } }

      client = instance_double(Gitlab::Client)
      expect(client).to receive(:block_user).with(1)

      u = SalsaLdapsync::GitlabUser.new(client, info)
      expect(u.state = 'blocked').to eq('blocked')
    end

    it 'unblocks user' do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'blocked' }
      allow(info).to receive(:to_h) { { 'custom_attributes' => [] } }

      client = instance_double(Gitlab::Client)
      expect(client).to receive(:unblock_user).with(1)

      u = SalsaLdapsync::GitlabUser.new(client, info)
      expect(u.state = 'active').to eq('active')
    end
  end

  describe '.sshkeys_id' do
    it do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'active' }
      allow(info).to receive(:to_h) { {
        'custom_attributes' => [
          { 'key' => 'synced-sshkey-1', 'value' => 'ssh-rsa key' },
        ]
      } }

      client = instance_double(Gitlab::Client)

      u = SalsaLdapsync::GitlabUser.new(client, info)
      expect(u.sshkeys_id.length).to eq(1)
    end
  end

  describe '.create_sshkeys' do
    it 'creates key and custom attribute' do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'active' }
      allow(info).to receive(:to_h) { {} }

      info_key = double()
      allow(info_key).to receive(:id) { 1 }

      client = instance_double(Gitlab::Client)
      expect(client).to receive(:create_ssh_key) do |title, key, options|
        expect(title).not_to be_nil
        expect(key).to eq('ssh-rsa key')
        expect(options).to eq({:user_id => 1})
        info_key
      end
      expect(client).to receive(:set_user_custom_attribute).with(1, 'synced-sshkey-1', 'ssh-rsa key')

      u = SalsaLdapsync::GitlabUser.new(client, info)
      u.create_sshkey(SalsaLdapsync::SshKey.from_string('ssh-rsa key comment'))
    end
  end

  describe '.delete_sshkeys' do
    it 'deletes key and custom attribute' do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'active' }
      allow(info).to receive(:to_h) { {
        'custom_attributes' => [
          { 'key' => 'synced-sshkey-1', 'value' => 'ssh-rsa key' },
        ]
      } }

      info_key = double()
      allow(info_key).to receive(:id) { 1 }

      client = instance_double(Gitlab::Client)
      expect(client).to receive(:delete_ssh_key).with(1, { :user_id => 1 })
      expect(client).to receive(:delete_user_custom_attribute).with(1, 'synced-sshkey-1')

      u = SalsaLdapsync::GitlabUser.new(client, info)
      u.delete_sshkey(SalsaLdapsync::SshKey.from_string('ssh-rsa key comment'))
    end
  end

  describe '.sshkeys=' do
    it 'calls .create_sshkey and .delete_sshkey' do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'active' }
      allow(info).to receive(:to_h) { {
        'custom_attributes' => [
          { 'key' => 'synced-sshkey-1', 'value' => 'ssh-rsa key1' },
          { 'key' => 'synced-sshkey-2', 'value' => 'ssh-rsa key2' },
        ]
      } }

      client = instance_double(Gitlab::Client)

      u = SalsaLdapsync::GitlabUser.new(client, info)
      expect(u).to receive(:delete_sshkey) do |arg|
        expect(arg.type).to eq('ssh-rsa')
        expect(arg.key).to eq('key1')
      end
      expect(u).to receive(:delete_sshkey) do |arg|
        expect(arg.type).to eq('ssh-rsa')
        expect(arg.key).to eq('key2')
      end
      expect(u).to receive(:create_sshkey) do |arg|
        expect(arg.type).to eq('ssh-rsa')
        expect(arg.key).to eq('key3')
      end
      expect(u).to receive(:create_sshkey) do |arg|
        expect(arg.type).to eq('ssh-rsa')
        expect(arg.key).to eq('key4')
      end

      u.sshkeys = Set[
        SalsaLdapsync::SshKey.from_string('ssh-rsa key3'),
        SalsaLdapsync::SshKey.from_string('ssh-rsa key4'),
      ]
    end

    it 'swallows exceptions from .create_sshkey and .delete_sshkey' do
      info = double()
      allow(info).to receive(:id) { 1 }
      allow(info).to receive(:username) { 'user' }
      allow(info).to receive(:state) { 'active' }
      allow(info).to receive(:to_h) { {
        'custom_attributes' => [
          { 'key' => 'synced-sshkey-1', 'value' => 'ssh-rsa key1' },
        ]
      } }

      client = instance_double(Gitlab::Client)

      u = SalsaLdapsync::GitlabUser.new(client, info)
      expect(u).to receive(:create_sshkey).and_raise("boom")
      expect(u).to receive(:delete_sshkey).and_raise("boom")

      u.sshkeys = Set[
        SalsaLdapsync::SshKey.from_string('ssh-rsa key3'),
      ]
    end
  end
end

describe SalsaLdapsync::GitlabUser::CustomAttributes do
  describe '.initialize' do
    it do
      client = instance_double(Gitlab::Client)

      c = SalsaLdapsync::GitlabUser::CustomAttributes.new(client, 1, 'user', {'a' => 'a'})
      expect(c)
    end
  end

  describe '.[]' do
    it do
      client = instance_double(Gitlab::Client)

      c = SalsaLdapsync::GitlabUser::CustomAttributes.new(client, 1, 'user', {'a' => 'a'})
      expect(c['a']).to eq('a')
    end
  end

  describe '.[]=' do
    it do
      client = instance_double(Gitlab::Client)
      expect(client).to receive(:set_user_custom_attribute).with(1, 'a', 'b')

      c = SalsaLdapsync::GitlabUser::CustomAttributes.new(client, 1, 'user', {'a' => 'a'})
      c['a'] = 'b'
      expect(c['a']).to eq('b')
    end
  end

  describe '.delete' do
    it do
      client = instance_double(Gitlab::Client)
      expect(client).to receive(:delete_user_custom_attribute).with(1, 'a')

      c = SalsaLdapsync::GitlabUser::CustomAttributes.new(client, 1, 'user', {'a' => 'a'})
      expect(c.delete('a')).to eq('a')
      expect(c['a']).to be_nil
    end
  end
end
