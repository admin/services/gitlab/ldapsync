require 'salsa_ldapsync/sshkey'

describe SalsaLdapsync::SshKey do
  describe '.initialize' do
    it 'accepts ssh-rsa key type' do
      expect(SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key'))
    end

    it 'accepts ssh-ed2519 key type' do
      expect(SalsaLdapsync::SshKey.new(type: 'ssh-ed25519', key: 'key'))
    end

    it 'rejects ssh-dss key type' do
      expect { SalsaLdapsync::SshKey.new(type: 'ssh-dss', key: 'key') }.to raise_exception(ArgumentError)
    end

    it 'rejects unknown key type' do
      expect { SalsaLdapsync::SshKey.new(type: 'ssh-test', key: 'key') }.to raise_exception(ArgumentError)
    end

    it 'accepts comment' do
      expect(SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key', comment: 'comment'))
    end

    it 'accepts options' do
      expect(SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key', options: 'options'))
    end
  end

  describe '::from_string' do
    it 'supports plain key' do
      key = SalsaLdapsync::SshKey.from_string('ssh-rsa key')
      expect(key.type).to eq('ssh-rsa')
      expect(key.key).to eq('key')
      expect(key.comment).to eq(nil)
      expect(key.options).to eq(nil)
    end

    it 'supports comments' do
      key = SalsaLdapsync::SshKey.from_string('ssh-rsa key comment1 comment2')
      expect(key.type).to eq('ssh-rsa')
      expect(key.key).to eq('key')
      expect(key.comment).to eq('comment1 comment2')
      expect(key.options).to eq(nil)
    end

    it 'supports options' do
      key = SalsaLdapsync::SshKey.from_string('option1,option2="value 2" ssh-rsa key')
      expect(key.type).to eq('ssh-rsa')
      expect(key.key).to eq('key')
      expect(key.comment).to eq(nil)
      expect(key.options).to eq('option1,option2="value 2"')
    end

    it 'supports Debian LDAP allowed_hosts extension' do
      key = SalsaLdapsync::SshKey.from_string('allowed_hosts=a,b,c option1,option2="value 2" ssh-rsa key')
      expect(key.type).to eq('ssh-rsa')
      expect(key.key).to eq('key')
      expect(key.comment).to eq(nil)
      expect(key.options).to eq('allowed_hosts,option1,option2="value 2"')
    end

    it 'raises exception on unsupported keys' do
      expect { SalsaLdapsync::SshKey.from_string('ssh-unknown key') }.to raise_exception(ArgumentError)
    end
  end

  describe '.type' do
    it do
      expect(SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key').type).to eq('ssh-rsa')
    end
  end

  describe '.key' do
    it do
      expect(SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key').key).to eq('key')
    end
  end

  describe '.eql?' do
    it do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      expect(k1.eql?(k2)).to be_truthy
    end

    it 'does compare .type' do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-ed25519', key: 'key')
      expect(k1.eql?(k2)).not_to be_truthy
    end

    it 'does compare .key' do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key1')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key2')
      expect(k1.eql?(k2)).not_to be_truthy
    end

    it 'does not compare .comment' do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key', 'comment': 'k2')
      expect(k1.eql?(k2)).to be_truthy
    end

    it 'does not compare .options' do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key', 'options': 'k2')
      expect(k1.eql?(k2)).to be_truthy
    end
  end

  describe '.hash' do
    it do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      expect(k1.hash).to eq(k2.hash)
    end

    it 'does compare .type' do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-ed25519', key: 'key')
      expect(k1.hash).not_to eq(k2.hash)
    end

    it 'does compare .key' do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key1')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key2')
      expect(k1.hash).not_to eq(k2.hash)
    end

    it 'does not compare .comment' do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key', 'comment': 'k2')
      expect(k1.hash).to eq(k2.hash)
    end

    it 'does not compare .options' do
      k1 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key')
      k2 = SalsaLdapsync::SshKey.new(type: 'ssh-rsa', key: 'key', 'options': 'k2')
      expect(k1.hash).to eq(k2.hash)
    end
  end
end
